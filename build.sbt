
name := "NBD_Scala"

version := "1.0"

scalaVersion := "2.11.2"

javaOptions += "-Xmx2048m"    // Doesn't work with IntelliJ. Add -Xmx in Run Configuration -> VM options

libraryDependencies ++= Seq(
  "com.typesafe.akka" %% "akka-actor" % "2.3.6",
  "org.scalatest" %% "scalatest" % "2.2.1" % "test"
)
