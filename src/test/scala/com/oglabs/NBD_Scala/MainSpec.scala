package com.oglabs.NBD_Scala

import org.scalatest.{Matchers, FunSpec}

/**
 * Created by Oskar Gargas on 04.10.14.
 */
class MainSpec extends FunSpec with Matchers {
  describe("Main object") {
    it("should be an App") {
      Main shouldBe an [App]
    }
  }
}
