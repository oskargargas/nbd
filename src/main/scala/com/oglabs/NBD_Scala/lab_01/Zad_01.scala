package com.oglabs.NBD_Scala.lab_01

/**
 * Created by Oskar Gargas on 09.10.14.
 */
class Zad_01 {
  println("--- Zadanie 1 ---")

  private val dniTygodnia = List("poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota", "niedziela")

  def podpunktA(): Unit = {
    println("\n--- Podpunkt A ---")
    for(d <- dniTygodnia) println(d)
  }

  def podpunktB(): Unit = {
    println("\n--- Podpunkt B ---")
    for(d <- dniTygodnia)
      if (d.startsWith("p"))
        println(d)
  }

  def podpunktC(): Unit = {
    println("\n--- Podpunkt C ---")
    dniTygodnia.foreach(println _)
  }

  def podpunktD(): Unit = {
    println("\n--- Podpunkt D ---")
    var i = 0
    while(i < dniTygodnia.length) {
      println(dniTygodnia(i))
      i += 1
    }
  }

  def podpunktE(): Unit = {
    println("\n--- Podpunkt E ---")

    def rekurencyjnie(lista: List[String]): Unit = {
      if (lista.nonEmpty) {
        println(lista.head)
        rekurencyjnie(lista.tail)
      }
    }

    rekurencyjnie(dniTygodnia)
  }

  def podpunktF(): Unit = {
    println("\n--- Podpunkt F ---")

    def rekurencyjnie(lista: List[String]): Unit = {
      if (lista.nonEmpty) {
        println(lista.last)
        rekurencyjnie(lista.init)
      }
    }

    rekurencyjnie(dniTygodnia)
  }

  def podpunktG(): Unit = {
    println("\n--- Podpunkt G ---")
    val foldL = dniTygodnia.foldLeft("")((b, a) => b + a + '\n')
    val foldR = dniTygodnia.foldRight("")((a, b) => a + '\n' + b)
    print("-- foldLeft\n" + foldL)
    print("-- foldRight\n" + foldR)
  }

  def podpunktH(): Unit = {
    println("\n--- Podpunkt H ---")
    val foldL = dniTygodnia.filter(_.startsWith("p")).foldLeft("")((b, a) => b + a + '\n')
    print(foldL)
  }
}
