package com.oglabs.NBD_Scala.lab_01

/**
 * Created by Oskar Gargas on 09.10.14.
 */
class Osoba(var firstName: String, var lastName: String) {

}

object Osoba {
  def powitanie(person: Osoba): String = {
    person match {
      case oskar if person.firstName == "Oskar" => "Witaj Oskar! Miło Cię widzieć!"
      case gargas if person.lastName == "Gargas" => "Witamy ponownie osobę z rodziny Gargasów!"
      case _ => "Witaj " + person.firstName + " " + person.lastName + "!"
    }
  }
}
