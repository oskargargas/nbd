package com.oglabs.NBD_Scala.lab_01

/**
 * Created by Oskar Gargas on 09.10.14.
 */
class KontoBankowe(private var stanKonta: Double = 0) {
  def getStanKonta = stanKonta

  def wplata(wartosc: Double): Unit = {
    this.stanKonta += wartosc
  }

  def wyplata(wartosc: Double): Unit = {
    this.stanKonta -= wartosc
  }
}
