package com.oglabs.NBD_Scala.lab_01

/**
 * Created by Oskar Gargas on 10.10.14.
 */

object Laboratorium_01 {
  def rozwiązania() = {
    val zad01 = new Zad_01
    zad01.podpunktA()
    zad01.podpunktB()
    zad01.podpunktC()
    zad01.podpunktD()
    zad01.podpunktE()
    zad01.podpunktF()
    zad01.podpunktG()
    zad01.podpunktH()

    val zad02do05i08 = new Zad_02_do_05_i_08
    zad02do05i08.zad02()
    zad02do05i08.zad03()
    zad02do05i08.zad04()
    zad02do05i08.zad05()

    println("\n--- Zadanie 6 ---")

    val pusteKonto = new KontoBankowe()
    val konto = new KontoBankowe(3000)

    println("Puste konto: " + pusteKonto.getStanKonta)
    println("Konto: " + konto.getStanKonta)
    konto.wplata(5000)
    konto.wyplata(1000)
    println("Konto po wpłacie i wypłacie: " + konto.getStanKonta)

    println("\n--- Zadanie 7 ---")

    val osoby = List(new Osoba("Oskar", "Asd"), new Osoba("Michał", "Gargas"), new Osoba("Lorem", "Ipsum"), new Osoba("Dolor", "Sit"), new Osoba("Amet", "Consectetur"))
    osoby.foreach(o => println(Osoba powitanie o))

    zad02do05i08.zad08()

    val zad09 = new Zad_09
    zad09.rozwiazanie()
  }
}
