package com.oglabs.NBD_Scala.lab_01

import akka.actor._
import akka.routing.RoundRobinRouter

import scala.util.Random
import scala.concurrent.duration._

/**
 * Created by Oskar Gargas on 10.10.14.
 */

class Zad_09 {
  private val random = new Random()
  private val actorSystem = ActorSystem("ArithmeticMean")

  def rozwiazanie(): Unit = {
    println("\n--- Zadanie 9 ---")
    calculate(4, 100000137, 10000000)    // step <= 2.147.000.000 pozwala na użycie Int do sumowania liczb z zakresu 1-100
  }

  private def calculate(numberOfWorkers: Int, numbers: Int, step: Int): Unit = {
    val listener = actorSystem.actorOf(Props(new ResultListener), name = "listener")
    val master = actorSystem.actorOf(Props(new Master(numberOfWorkers, generateNumbers(numbers), step, listener)), name = "master")

    master ! Calculate
  }

  private def generateNumbers(n: Int): Array[Byte] = {  // Liczby z zakresu 1-100 mieszczą się na 8 bitach.
    Array.fill(n)((random.nextInt(100) + 1).toByte)     // Nie ma potrzeby stosowania 32 bitowego typu Int.
  }                                                     // ~2,86 MB oszczędności RAM na 1.000.000 liczb.

  private sealed trait ArithmeticMeanMessage
  private case object Calculate extends ArithmeticMeanMessage
  private case class Work(numbers: Array[Byte]) extends ArithmeticMeanMessage
  private case class Result(value: Long) extends ArithmeticMeanMessage
  private case class ArithmeticMean(value: Double, duration: Long) extends ArithmeticMeanMessage

  private class Worker extends Actor {
    override def receive: Receive = {
      case Work(numbers) =>
        sender ! Result(calculateArithmeticMean(numbers))
    }

    private def calculateArithmeticMean(numbers: Array[Byte]): Int = {
      var sum: Int = 0                         // Można użyć foldLeft/Right ale tak jest czytelniej
      numbers.foreach(sum += _)
      sum
    }
  }

  private class Master(nrOfWorkers: Int, numbers: Array[Byte], step: Int, listener: ActorRef) extends Actor {
    val startTime: Long = System.currentTimeMillis
    var sum: Long = _

    val workerRouter = context.actorOf(Props(new Worker).withRouter(RoundRobinRouter(nrOfWorkers)), name = "workerRouter")

    var numberOfMessages: Int = if (numbers.length % step == 0) numbers.length / step else numbers.length / step + 1
    var numberOfResults: Int = _

    override def receive: Receive = {
      case Calculate =>
        for (i <- 0 until(numbers.length, step)) workerRouter ! Work(numbers.slice(i, i + step))
      case Result(value) =>
        sum += value
        numberOfResults += 1

        if (numberOfResults == numberOfMessages) {
          listener ! ArithmeticMean(sum / numbers.length.toDouble, System.currentTimeMillis - startTime)

          import actorSystem.dispatcher
          actorSystem.scheduler.scheduleOnce(1 second) {
            self ! PoisonPill
          }
        }
    }
  }

  private class ResultListener extends Actor {
    override def receive: Actor.Receive = {
      case ArithmeticMean(value, duration) =>
        println(s"Arithmetic mean: $value, Duration: $duration")
        context.system.shutdown()
    }
  }

}
