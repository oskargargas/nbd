package com.oglabs.NBD_Scala.lab_01

/**
 * Created by Oskar Gargas on 09.10.14.
 */
class Zad_02_do_05_i_08 {
  private val produkty = Map("monitor" -> 1000.00, "klawiatura" -> 119.99)
  private val dniTygodnia = List("poniedziałek", "wtorek", "środa", "czwartek", "piątek", "sobota", "niedziela")

  def zad02(): Unit = {
    println("\n--- Zadanie 2 ---")

    val przecena = produkty.mapValues(_ * 0.9)

    println("Produkty: " + produkty)
    println("Przecena: " + przecena)
  }


  def zad03(): Unit = {
    println("\n--- Zadanie 3 ---")

    def fPrzyjmującaKrotkę[A, B, C](t: (A, B, C)): Unit = {
      println("(" + t._1 + ", " + t._2 + ", " + t._3 + ")")
    }

    fPrzyjmującaKrotkę(Tuple3("Lorem", 123f, 445))
  }



  def zad04(): Unit = {
    println("\n--- Zadanie 4 ---")

    def contains(map: Map[String, Double], key: String): Option[Double] = {
      if (map.contains(key))
        Some(map(key))
      else
        None
    }

    val a = contains(produkty, "monitor")
    val b = contains(produkty, "telefon")

    val alternate: String = "Brak produktu"
    println(a.getOrElse(alternate))
    println(b.getOrElse(alternate))
  }

  def zad05(): Unit = {
    println("\n--- Zadanie 5 ---")

    def czyPraca(d: String): String = {
      d.toLowerCase match {
        case p if dniTygodnia.slice(0, 5).contains(p) => "Praca"
        case w if dniTygodnia.slice(5, dniTygodnia.length).contains(w) => "Weekend"
        case _ => "Nie ma takiego dnia"
      }
    }

    dniTygodnia.foreach(d => println(d + ": " + czyPraca(d)))
    println("Komputer" + ": " + czyPraca("Komputer"))
  }

  def zad08(): Unit = {
    println("\n--- Zadanie 8 ---")

    val listaWartościCałkowitych = List[Int](1, 0, 123, 3, 0, 0, 12, 12, 67)

    def usuwanieZer(lista: List[Int]): List[Int] = {
      lista.filterNot(_ == 0)
    }

    println("listaWartościCałkowitych: " + listaWartościCałkowitych)
    println("usunięte zera: " + usuwanieZer(listaWartościCałkowitych))
  }

}
